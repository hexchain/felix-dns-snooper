package main

import (
	"fmt"
	"flag"

	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
)

var iface = flag.String("i", "eth0", "Interface to read packets from")

func handlePacket(packet *gopacket.Packet) {
	if dnsLayer := (*packet).Layer(layers.LayerTypeDNS); dnsLayer != nil {
		dns, _ := dnsLayer.(*layers.DNS)
		if len(dns.Questions) > 0 {
			fmt.Println(string(dns.Questions[0].Name))
		}
	}
}

func main() {
	flag.Parse()
	fmt.Println("interface:", *iface)

	if handle, err := pcap.OpenLive(*iface, 1600, true, pcap.BlockForever); err != nil {
		panic(err)
	} else if err := handle.SetBPFFilter("udp and dst port 53"); err != nil {
		panic(err)
	} else {
		packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
		for packet := range packetSource.Packets() {
			handlePacket(&packet)
		}
	}
}
